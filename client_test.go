/*
Copyright 2016 Mike Lloyd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fiveq

import (
	"os"
	"testing"
)

var (
	cis, cid    string
	localClient *FiveQClient
)

func TestMain(m *testing.M) {
	lcid := os.Getenv("FIVEQ_CLIENT_ID")
	lcis := os.Getenv("FIVEQ_CLIENT_SECRET")
	cis = lcis
	cid = lcid

	os.Exit(m.Run())
}

func TestNewClient(t *testing.T) {
	lc := NewClient(cid, cis)
	localClient = lc
}

// Here is how you would make a new 5Q client and a high-level API call to the keyboard via the remote endpoint.
func ExampleNewClient() {
	client := NewClient("ClientID", "ClientSecret")
	_, err := client.GetAuthorizedClients()
	if err != nil {
		panic(err)
	}
	// validate clients are allowed or something nifty.
}

func TestFiveQClient_GetAuthorizedClients(t *testing.T) {
	aclients, err := localClient.GetAuthorizedClients()
	if err != nil {
		t.Log(err)
		t.Fail()
	}

	for _, i := range aclients {
		t.Logf("Seen client: %s", i.Name)
	}
}

func TestFiveQClient_GetColors(t *testing.T) {
	lcolours, err := localClient.GetColors()
	if err != nil {
		t.Log(err)
		t.Fail()
	}

	for _, i := range lcolours {
		t.Logf("Colour %s is available.", i.Name)
	}
}
