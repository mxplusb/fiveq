/*
Package fiveq is an unofficial client library for Das Keyboard's 5Q Cloud Connected Keyboard. This library is threadsafe and licensed under the Apache License Version 2.0.

Do you think keyboards as input devices only? With this project, we are redefining the way we use keyboards by adding an output function. You can now also use it as a dashboard. Intrigued? Read on.

Das Keyboard 5Q is a cloud-connected, open API RGB mechanical keyboard that allows each key to be color-controlled over the Internet. It is built with ground-breaking electronics that provide superior RGB LED brightness and unsurpassed response time. Along with the Das Keyboard Q application, the Das Keyboard 5Q makes you more productive by streaming information directly to your keyboard.

For more information on the keyboard, see: https://www.kickstarter.com/projects/1229573443/das-keyboard-5q-the-cloud-connected-keyboard.

*/
package fiveq
