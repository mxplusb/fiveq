/*
Copyright 2016 Mike Lloyd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fiveq

const (
	BLUE  = "#00F"
	GREEN = "#0F0"
	RED   = "#F00"
)

// AuthorizedClients represents how many clients have been authourised to use the 5Q software.
type AuthorizedClients struct {
	Name string `json:"name"`
}

// DeviceDefinition represents the API response of the 5Q keyboard specifications, such as it's PID, ModelNumber, etc.
type DeviceDefinition struct {
	LatestFirmwareVersion string `json:"latestFirmwareVersion"`
	Name                  string `json:"name"`
	Vid                   string `json:"vid"`
	Pid                   string `json:"pid"`
	ModelNumber           string `json:"modelNumber"`
	Description           string `json:"description"`
	Zones                 []struct {
		ID          string `json:"id"`
		Description string `json:"description"`
	} `json:"zones"`
	VidPid struct {
		Pid string `json:"pid"`
		Vid string `json:"vid"`
	} `json:"vidPid"`
}

// Colors represents the basic color structure the keyboard can display.
type Colors struct {
	Code string `json:"code"`
	Name string `json:"name"`
}
