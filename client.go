/*
Copyright 2016 Mike Lloyd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fiveq

import (
	"context"
	"encoding/json"
	"golang.org/x/oauth2/clientcredentials"
	"net/http"
)

const (
	// TOKEN_URL is the base OAuth 2.0 URL for tokens.
	TOKEN_URL = "http://q.daskeyboard.com/oauth/1.4/token"

	// DEVICE_DEF is the endpoint for your or others keyboard specifications.
	DEVICE_DEF = "http://q.daskeyboard.com/api/1.0/device_definitions"

	// AUTHOURISED_CLIENTS is the endpoint for reviewing clients which have access to your 5Q software.
	AUTHORISED_CLIENTS = "http://q.daskeyboard.com/api/1.0/users/authorized_clients"

	// COLORS is the endpoint for pulling available colours from the API.
	COLORS = "http://q.daskeyboard.com/api/1.0/colors"
)

// FiveQClient is the base client for all local and remote 5Q calls. It is threadsafe and will refresh the token. It embeds an HTTP client with OAuth 2.0, so you can use the native API calls or make extremely low level calls if needed.
type FiveQClient struct {
	*http.Client
	*Mine
}

type Mine struct {
	MyPID string
}

// NewClient gives a new, yet unauthorised client for use. Use this to make native API calls or low-level calls. OAuth 2.0 is included.
func NewClient(cid, cis string) *FiveQClient {
	tconf := clientcredentials.Config{
		ClientID:     cid,
		ClientSecret: cis,
		TokenURL:     TOKEN_URL,
	}

	c := tconf.Client(context.Background())
	return &FiveQClient{c, &Mine{}}
}

// GetAuthorizedClients returns a list of AuthorizedClients which have been allowed access to the 5Q software.
func (f *FiveQClient) GetAuthorizedClients() ([]AuthorizedClients, error) {
	var ldevs []AuthorizedClients
	resp, err := f.Get(AUTHORISED_CLIENTS)
	if err != nil {
		return []AuthorizedClients{}, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&ldevs)
	if err != nil {
		return []AuthorizedClients{}, nil
	}

	return ldevs, nil
}

// GetColors returns a list of AuthorizedClients which have been allowed access to the 5Q software.
func (f *FiveQClient) GetColors() ([]Colors, error) {
	var lcolours []Colors
	resp, err := f.Get(COLORS)
	if err != nil {
		return []Colors{}, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&lcolours)
	if err != nil {
		return []Colors{}, nil
	}

	return lcolours, nil
}
